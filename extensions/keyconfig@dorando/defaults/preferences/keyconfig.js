pref("keyconfig.devmode", false);
pref("keyconfig.nicenames.reverse_order", false);
pref("keyconfig.profile", "main");
pref("keyconfig.warnOnDuplicate", true);
pref("keyconfig.allowAltCodes", true);
pref("keyconfig.global.20110522", "Module('Detect');");
pref("keyconfig.UIHook", "");
pref("keyconfig.hideDisabled", false);
