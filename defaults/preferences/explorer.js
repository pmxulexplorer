pref("toolkit.defaultChromeURI", "chrome://explorer/content/explorer.xul");

/* https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XUL/prefwindow */
pref("browser.preferences.instantApply", false); /* https://bugzilla.mozilla.org/show_bug.cgi?id=485150 */
pref("browser.preferences.animateFadeIn", false);

/* debugging prefs */
pref("browser.dom.window.dump.enabled", true);
pref("javascript.options.showInConsole", true);
pref("javascript.options.strict", false);
pref("nglayout.debug.disable_xul_cache", true);
pref("nglayout.debug.disable_xul_fastload", true);

/* added to allow <label class="text-links" ... /> to work */
pref("network.protocol-handler.expose.http", false);
pref("network.protocol-handler.warn-external.http", false);

/* extension prefs - turn off extension updating for now */
pref("extensions.update.enabled", false);

/* explorer prefs */
pref("explorer.startup.script", "Window");
pref("explorer.startup.category", "");
pref("explorer.startup.sidebar", "");
pref("explorer.preview.autorefresh", false);
pref("explorer.snippets.user", "");
pref("explorer.manifest.locations", "");
pref("explorer.extension.locations", "");
pref("explorer.application.locations", "");

/* editor prefs */
pref("sourceeditor.baseURI", "chrome://explorer-orion/content/");
pref("sourceeditor.tabsize", 2);
pref("sourceeditor.expandtab", true);

/* application update prefs */
pref("app.update.channel", "https://127.0.0.1");
pref("app.update.enabled", false);
pref("app.update.auto", false); // auto download updates
pref("app.update.mode", 1); // prompt for incompatible add-ons
pref("app.update.url", "https://127.0.0.1/aus/3/%PRODUCT%/%VERSION%/%BUILD_ID%/%BUILD_TARGET%/%LOCALE%/%CHANNEL%/%OS_VERSION%/%DISTRIBUTION%/%DISTRIBUTION_VERSION%/update.xml");
pref("app.update.url.manual", "http://developer.mozilla.org/en/XUL_Explorer");
pref("app.update.url.details", "http://developer.mozilla.org/en/XUL_Explorer");
pref("app.update.interval", 86400); // check once a day
pref("app.update.nagTimer.download", 86400);
pref("app.update.nagTimer.restart", 600);
pref("app.update.timer", 60000); // 1 minute
pref("app.update.showInstalledUI", false); // broken?
