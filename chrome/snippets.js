Components.utils.import("chrome://explorer/content/modules/utils.js");

let SnippetManager = {
  app: null,
  snippets: [],
  categories: [],

  init: function (aApp, aChromeFile, aUserFile) {
    this.app = aApp;
    this.snippets = eval("("+readTextFile(aChromeFile)+")");
    try {
      if (aUserFile && aUserFile.length > 0) {
        let userFile = "file:///"+aUserFile;
        userSnippets = eval("("+readTextFile(userFile)+")");
        for each (let snippet in userSnippets) this.snippets.push(snippet);
      }
    } catch(e) {}

    for each (let snippet in this.snippets) {
      if (this.categories.indexOf(snippet.category) == -1) this.categories.push(snippet.category);
    }

    this.categories.sort(function (a, b) (a < b ? -1 : 1));
  },

  filterByCategory: function (categoryIndex) {
    let filtered = [];
    for (let i in this.snippets) {
      if (categoryIndex == -1 || this.snippets[i].category == this.categories[categoryIndex]) filtered.push(i);
    }
    return filtered;
  }
};
