////////////////////////////////////////////////////////////////////////////////
this.EXPORTED_SYMBOLS = [
  "readTextFile"
];

////////////////////////////////////////////////////////////////////////////////
//Components.utils.import("chrome://explorer/content/modules/debuglog.js");


////////////////////////////////////////////////////////////////////////////////
const {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;


////////////////////////////////////////////////////////////////////////////////
function readTextFile (fileName) {
  let res = "";
  try {
    // can't use `XMLHttpRequest` in code module: there's no `window` object to get it from
    //let req = new XMLHttpRequest({mozSystem:true});
    let req = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance(Ci.nsIXMLHttpRequest);
    req.mozBackgroundRequest = true;
    req.open("GET", fileName, false);
    req.channel.loadFlags |=
      Ci.nsIRequest.INHIBIT_CACHING|
      Ci.nsIRequest.INHIBIT_PERSISTENT_CACHING|
      Ci.nsIRequest.LOAD_BYPASS_CACHE|
      Ci.nsIRequest.LOAD_ANONYMOUS|
      0;
    req.overrideMimeType("text/plain;charset=UTF-8");
    req.send(null);
    res = req.responseText;
  } catch (e) {}
  //conlog("'", fileName, "': ", res);
  // fuck BOM
  if (res.length >= 3 && res.substr(0, 3) == "\u00EF\u00BB\u00BF") res = res.substr(3);
  return res;
}
