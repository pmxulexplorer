////////////////////////////////////////////////////////////////////////////////
this.EXPORTED_SYMBOLS = [
  "conlog",
  "logError"
];

////////////////////////////////////////////////////////////////////////////////
const {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;

////////////////////////////////////////////////////////////////////////////////
const consvc = Components.classes['@mozilla.org/consoleservice;1'].getService(Components.interfaces.nsIConsoleService);


////////////////////////////////////////////////////////////////////////////////
function conlog () {
  if (arguments.length > 0) {
    let s = "";
    for (let idx = 0; idx < arguments.length; ++idx) s += arguments[idx];
    if (s.length) consvc.logStringMessage(s);
  }
}


function logError () {
  if (arguments.length > 0) {
    let s = "";
    for (let idx = 0; idx < arguments.length; ++idx) s += arguments[idx];
    if (s.length) Cu.reportError(s);
  }
}
