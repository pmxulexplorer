function cancelEvent (evt) {
  if (evt) {
    if (typeof(evt.preventDefault) === "function") evt.preventDefault();
    if (typeof(evt.stopPropagation) === "function") evt.stopPropagation();
    if (typeof(evt.stopImmediatePropagation) === "function") evt.stopImmediatePropagation();
  }
}
