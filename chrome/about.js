window.addEventListener("load", function () {
  //if (evt.target != document) return;
  let appInfo = Components.classes["@mozilla.org/xre/app-info;1"].getService(Components.interfaces.nsIXULAppInfo);
  let version = document.getElementById("version");
  version.value = appInfo.name+" "+appInfo.version;
  let userAgent = document.getElementById("useragent");
  userAgent.value = navigator.userAgent;
}, false);
