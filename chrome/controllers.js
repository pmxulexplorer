////////////////////////////////////////////////////////////////////////////////
let fileController = {
  _app: null,

  init: function (aApp) {
    this._app = aApp;
  },

  supportsCommand: function (cmd) {
    switch (cmd) {
      case "cmd_newBlank":
      case "cmd_newExtension":
      case "cmd_newApplication":
      case "cmd_open":
      case "cmd_save":
      case "cmd_saveAs":
      case "cmd_close":
      case "cmd_pageSetup":
      case "cmd_print":
      case "cmd_exit":
        return true;
    }
    return false;
  },

  isCommandEnabled: function (cmd) {
    if (cmd === "cmd_close") {
      let tabs = document.getElementById("workspace_tabs");
      let tabElems = tabs.getElementsByTagName("tab");
      return (tabElems.length > 1);
    }
    return true;
  },

  _doOpen: function () {
    /* See: http://developer.mozilla.org/en/docs/XUL_Tutorial:Open_and_Save_Dialogs */
    let nsIFilePicker = Components.interfaces.nsIFilePicker;
    let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
    fp.init(window, "Open File", nsIFilePicker.modeOpen);
    fp.appendFilters(nsIFilePicker.filterText|nsIFilePicker.filterAll);
    let res = fp.show();
    if (res === nsIFilePicker.returnOK) {
      let source = fileToString(fp.file);
      this._app.addEditor(fp.file.leafName, fp.file.path, source);
    }
  },

  _doSave: function (cmd) {
    let textEditor = this._app.getCurrentEditor();
    if (!textEditor) return;
    let fileName = this._app.getEditorFileName(textEditor);
    if (cmd === "cmd_saveAs" || !fileName) {
      /* See: http://developer.mozilla.org/en/docs/XUL_Tutorial:Open_and_Save_Dialogs */
      let nsIFilePicker = Components.interfaces.nsIFilePicker;
      let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
      fp.init(window, "Save File", nsIFilePicker.modeSave);
      fp.appendFilters(nsIFilePicker.filterText | nsIFilePicker.filterAll);
      let res = fp.show();
      if (res == nsIFilePicker.returnOK || res == nsIFilePicker.returnReplace) {
        fileName = fp.file.path;
      } else {
        // user wants to cancel
        return;
      }
    }

    if (fileName) {
      let file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
      file.initWithPath(fileName);
      let source = textEditor.getText();
      stringToFile(source, file);
      this._app.renameEditor(this._app.getCurrentEditor(), file.leafName, file.path, true);
    }
  },

  _doClose: function () {
    let editor = this._app.getCurrentEditor();
    if (editor) this._app.removeEditor(editor);
  },

  _doExit: function () {
    if (!this._app.shutdownQuery()) return;
    let aForceQuit = false;
    let appStartup = Components.classes['@mozilla.org/toolkit/app-startup;1'].getService(Components.interfaces.nsIAppStartup);
    // eAttemptQuit will try to close each XUL window, but the XUL window can cancel the quit
    // process if there is unsaved data. eForceQuit will quit no matter what.
    let quitSeverity = (aForceQuit ? Components.interfaces.nsIAppStartup.eForceQuit : Components.interfaces.nsIAppStartup.eAttemptQuit);
    appStartup.quit(quitSeverity);
  },

  doCommand: function (cmd) {
    switch (cmd) {
      case "cmd_newBlank":
        this._app.addEditor(null, null, null);
        break;
      case "cmd_newExtension":
        window.openDialog("chrome://explorer/content/tools/extension-project.xul", "extproject", "centerscreen,modal");
        break;
      case "cmd_newApplication":
        window.openDialog("chrome://explorer/content/tools/application-project.xul", "appproject", "centerscreen,modal");
        break;
      case "cmd_open":
        this._doOpen();
        break;
      case "cmd_saveAs":
      case "cmd_save":
        this._doSave(cmd);
        break;
      case "cmd_close":
        this._doClose();
        break;
      case "cmd_pageSetup":
        PrintUtils.showPageSetup();
        break;
      case "cmd_print":
        PrintUtils.print();
        break;
      case "cmd_exit":
        this._doExit();
        break;
    }
  },
};


////////////////////////////////////////////////////////////////////////////////
let editController = {
  _app: null,

  init: function (aApp) {
    this._app = aApp;
  },

  supportsCommand: function(cmd) {
    switch (cmd) {
      case "cmd_pasteNoFormatting":
        return true;
    }
    return false;
  },

  isCommandEnabled: function (cmd) {
    let textEditor = this._app.getCurrentEditor();
    return (textEditor ? !textEditor.readOnly : false);
  },

  doCommand: function (cmd) {
    let textEditor = this._app.getCurrentEditor();
    if (textEditor) {
      conlog("TODO: paste!");
    }
  },
};


////////////////////////////////////////////////////////////////////////////////
let viewController = {
  _app: null,

  init: function (aApp) {
    this._app = aApp;
  },

  supportsCommand: function (cmd) {
    switch (cmd) {
      case "cmd_sidebar":
      case "cmd_messages":
      case "cmd_refresh":
      case "cmd_preview":
      case "cmd_formatter":
        return true;
    }
    return false;
  },

  isCommandEnabled: function (cmd) true,

  _doFormat: function () {
    let formatter = new XMLFormatter();
    //formatter.indent = " "; //k8: one space
    this._app.setSource(formatter.format(this._app.getSource()));
  },

  _doRefresh: function () {
    this._app.refreshPreview();
  },

  doCommand: function (cmd) {
    switch (cmd) {
      case "cmd_sidebar":
        this._app.toggleSidebar(null, null);
        break;
      case "cmd_messages":
        this._app.toggleMessages(null, null);
        break;
      case "cmd_refresh":
        this._doRefresh();
        break;
      case "cmd_preview":
        this._app.openPreview();
        break;
      case "cmd_formatter":
        this._doFormat();
        break;
    }
  },
};


////////////////////////////////////////////////////////////////////////////////
let toolsController = {
  _app: null,

  init: function (aApp) {
    this._app = aApp;
  },

  supportsCommand: function(cmd) {
    switch (cmd) {
      case "cmd_checker":
      case "cmd_console":
      case "cmd_testExtension":
      case "cmd_addons":
      case "cmd_reloadChrome":
      case "cmd_options":
        return true;
    }
    return false;
  },

  isCommandEnabled: function (cmd) true,

  _doChecker: function () {
    let source = this._app.getSource();
    XULChecker.checkString(source, true, true);
    this._app.showOutput(XULChecker.outputLog);
  },

  _doConsole: function () {
    Components.utils.import("resource://gre/modules/Services.jsm");
    let inType = "global:console";
    let uri = "chrome://global/content/console.xul";
    let topWindow = Services.wm.getMostRecentWindow(inType);
    if (topWindow) {
      //topWindow.focus();
    } else {
      let win = this._app._window;
      win.open(uri, "_blank", "chrome,extrachrome,dependent,menubar,resizable,scrollbars,status,toolbar");
      win.focus();
    }
  },

  _doAddons: function () {
    const EMTYPE = "Extension:Manager";

    let aOpenMode = "extensions";
    let wm = Components.classes["@mozilla.org/appshell/window-mediator;1"].getService(Components.interfaces.nsIWindowMediator);
    let needToOpen = true;
    let windowType = EMTYPE+"-"+aOpenMode;
    let windows = wm.getEnumerator(windowType);
    while (windows.hasMoreElements()) {
      let theEM = windows.getNext().QueryInterface(Components.interfaces.nsIDOMWindowInternal);
      if (theEM.document.documentElement.getAttribute("windowtype") == windowType) {
        theEM.focus();
        needToOpen = false;
        break;
      }
    }

    if (needToOpen) {
      const EMURL = "chrome://mozapps/content/extensions/extensions.xul?type=" + aOpenMode;
      const EMFEATURES = "chrome,dialog=no,resizable=yes";
      window.openDialog(EMURL, "", EMFEATURES);
    }
  },

  doCommand: function (cmd) {
    switch (cmd) {
      case "cmd_checker":
        this._doChecker();
        break;
      case "cmd_console":
        this._doConsole();
        break;
      case "cmd_testExtension":
        window.openDialog("chrome://explorer/content/tools/extension-test.xul", "exttest", "centerscreen,modal");
        break;
      case "cmd_addons":
        this._doAddons();
        break;
      case "cmd_reloadChrome":
        try {
          Components.classes["@mozilla.org/chrome/chrome-registry;1"].getService(Components.interfaces.nsIXULChromeRegistry).reloadChrome();
        } catch (e) {}
        break;
      case "cmd_options":
        window.openDialog("chrome://explorer/content/options.xul", "options", "chrome,titlebar,toolbar,centerscreen,modal");
        break;
    }
  },
};


////////////////////////////////////////////////////////////////////////////////
let helpController = {
  _app: null,

  init: function (aApp) {
    this._app = aApp;
  },

  supportsCommand: function (cmd) {
    switch (cmd) {
      case "cmd_help":
      case "cmd_xulref":
      case "cmd_xultut":
      case "cmd_jsref":
      case "cmd_keyword":
      case "cmd_update":
      case "cmd_about":
        return true;
    }
    return false;
  },

  isCommandEnabled: function (cmd) true,

  doCommand: function (cmd) {
    switch (cmd) {
      case "cmd_help":
      case "cmd_xulref":
      case "cmd_xultut":
      case "cmd_keyword":
      case "cmd_jsref":
        window.openDialog("chrome://explorer/content/help/helpbrowser.xul", "_blank", "chrome,dependent,resizable,scrollbars", this._app.getHelpURI(cmd));
        break;
      case "cmd_update":
        this._app.openUpdates();
        break;
      case "cmd_about":
        window.openDialog("chrome://explorer/content/about.xul", "about", "centerscreen,modal");
        break;
    }
  },
};


////////////////////////////////////////////////////////////////////////////////
let messagesController = {
  _app: null,

  init: function (aApp) {
    this._app = aApp;
  },

  supportsCommand: function (cmd) {
    switch (cmd) {
      case "cmd_clearMessages":
        return true;
    }
    return false;
  },

  isCommandEnabled: function (cmd) true,

  doCommand: function (cmd) {
    switch (cmd) {
      case "cmd_clearMessages":
        this._app.clearMessages();
        break;
    }
  },
};
