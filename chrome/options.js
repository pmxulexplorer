let Options = {
  chooseSnippetsFolder: function () {
    const Cc = Components.classes, Ci = Components.interfaces;
    let fp = Cc["@mozilla.org/filepicker;1"].createInstance(Ci.nsIFilePicker);
    let strings = document.getElementById("strings_options");
    let title = strings.getString("userSnippets.dialog");
    fp.init(window, title, Ci.nsIFilePicker.modeOpen);
    fp.appendFilters(Ci.nsIFilePicker.filterAll);
    if (fp.show() == Ci.nsIFilePicker.returnOK) {
      let userSnippetPref = document.getElementById("pref_usersnippets");
      userSnippetPref.value = fp.file.path;
    }
  },

  chooseManifestsFolder: function () {
    const Cc = Components.classes, Ci = Components.interfaces;
    let fp = Cc["@mozilla.org/filepicker;1"].createInstance(Ci.nsIFilePicker);
    let strings = document.getElementById("strings_options");
    let title = strings.getString("userManifests.dialog");
    fp.init(window, title, Ci.nsIFilePicker.modeGetFolder);
    fp.appendFilters(Ci.nsIFilePicker.filterAll);
    if (fp.show() == Ci.nsIFilePicker.returnOK) {
      let txt = document.getElementById("manifests");
      txt.value = fp.file.path;
    }
  },

  addManifestsFolder: function () {
    let txt = document.getElementById("manifests");
    if (txt.value.length > 0) Options.addManifest(txt.value);
  },

  removeManifest: function (evt) {
    let listbox = document.getElementById("usermanifests_list");
    let target = listbox.selectedItem;
    let locations = listbox.getAttribute("value").split(";");
    locations = locations.filter(function(location) {
      return location != target.label;
    }, this);
    listbox.setAttribute("value", locations.join(";"));
    document.getElementById("paneManifests").userChangedValue(listbox);
  },

  addManifest: function (path) {
    let listbox = document.getElementById("usermanifests_list");
    let locations = listbox.getAttribute("value").split(";");
    if (locations.indexOf(path) == -1) {
      locations.push(path);
      listbox.setAttribute("value", locations.join(";"));
      document.getElementById("paneManifests").userChangedValue(listbox);
    }
  },

  manifestLocationsKeypress: function (evt) {
    if (evt.keyCode == KeyEvent.DOM_VK_DELETE) this.removeManifest(evt);
  },

  getManifestLocations: function () document.getElementById("usermanifests_list").getAttribute("value"),

  displayManifestLocations: function () {
    let listbox = document.getElementById("usermanifests_list");
    let prefValue = document.getElementById("paneManifests").preferenceForElement(listbox).value;
    let locations = prefValue.split(";");

    while (listbox.lastChild) listbox.removeChild(listbox.lastChild);

    locations.forEach(function (location) {
      if (location) {
        let listitem = document.createElement("listitem");
        listitem.setAttribute("label", location);
        listitem.label = location;
        listitem.setAttribute("context", "manifests_list_popup");
        listbox.appendChild(listitem);
      }
    }, this);

    listbox.setAttribute("value", locations.join(";"));
    return listbox.getAttribute("value");
  },
};
