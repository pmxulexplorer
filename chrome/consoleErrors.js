let ConsoleErrors = {
  app: null,

  startup: function (aApp) {
    this.app = aApp;
    let csClass = Components.classes['@mozilla.org/consoleservice;1'];
    let cs = csClass.getService(Components.interfaces.nsIConsoleService);
    cs.registerListener(this);
  },

  shutdown: function () {
    let csClass = Components.classes['@mozilla.org/consoleservice;1'];
    let cs = csClass.getService(Components.interfaces.nsIConsoleService);
    cs.unregisterListener(this);
  },

  update: function () {
    this.app.hasErrors(true);
  },

  observe: function (object) {
    try {
      let nsIScriptError = Components.interfaces.nsIScriptError;
      if (object instanceof nsIScriptError) {
        let isWarning = object.flags & nsIScriptError.warningFlag;
        if (!isWarning) this.update();
      } else {
        // Must be an nsIConsoleMessage
        this.update();
      }
    } catch (e) { }
  },
};
